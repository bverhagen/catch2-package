PREFIX:=package

all: prepare

clean:
	$(MAKE) --directory pkgbuild clean

# Package manager formats
pkgbuild:
	$(MAKE) --directory pkgbuild PREFIX=$(PREFIX) $(TARGET) 

# Distribution to package manager mapping
arch: pkgbuild

find-distribution:
	$(MAKE) $(shell lsb_release --id --short | tr A-Z a-z) TARGET=$(TARGET) PREFIX=$(PREFIX)

prepare:
	$(MAKE) find-distribution TARGET=prepare

build:
	$(MAKE) find-distribution TARGET=build

install:
	$(MAKE) find-distribution TARGET=install

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: pkgbuild arch find-distribution list all prepare
